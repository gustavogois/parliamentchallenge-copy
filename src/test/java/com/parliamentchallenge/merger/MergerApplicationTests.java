package com.parliamentchallenge.merger;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.parliamentchallenge.merger.service.ServicesNames;
import com.parliamentchallenge.merger.dto.SpeechesDTO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.util.MimeTypeUtils;

import java.util.Optional;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MergerApplicationTests {

	@Autowired
	private MockMvc mvc;

	@Autowired
	ObjectMapper objectMapper;

	@Test
	public void shouldStart() throws Exception {

		mvc.perform(get(ServicesNames.ROOT).accept(MediaType.APPLICATION_JSON))
				.andExpect(status().isOk())
				.andExpect(content().string(equalTo("Let the games begin!")));
	}

	@Test
	public void shouldReturnTenSpeechs() throws Exception {

		MvcResult mvcResult = mvc.perform(
				get(ServicesNames.RETRIEVE_LAST_TEN_SPEECHES).accept(MimeTypeUtils.APPLICATION_JSON_VALUE))
				.andReturn();
		String contentAsString = mvcResult.getResponse().getContentAsString();
		SpeechesDTO speechesDTO = objectMapper.readValue(contentAsString, SpeechesDTO.class);

		Integer speechListSize = Optional.ofNullable(speechesDTO.getSpeechesDTOS().size())
				.get();
		assertThat(speechListSize).isEqualTo(10);
	}

	// TODO: More tests, with JSons as inputs

}
