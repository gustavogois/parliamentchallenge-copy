package com.parliamentchallenge.merger.mapper;

import com.parliamentchallenge.merger.service.model.Member;
import com.parliamentchallenge.merger.service.model.Speech;
import com.parliamentchallenge.merger.dto.SpeechDTO;
import org.springframework.hateoas.Link;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

public class Mapper {

    // TODO: Unit tests
    public static List<SpeechDTO> speechesAndMembersToSpeechesDTO(List<Speech> speeches, Map<String,
            Member> membersSpeech) {

        List<SpeechDTO> speechesDTO = new ArrayList<>();

        for(Speech speech : speeches) {

            // TODO: catch any NPE and throw an adequate Exception
            String speechId = speech.getSpeechId();
            String dockDate = speech.getDockDate();
            String speaker = speech.getSpeaker();
            String party = Optional.ofNullable(speech.getParty()).orElse("");

            Member member = Optional.ofNullable(membersSpeech.get(speechId)).orElse(new Member());

            String officialEmail = Optional.ofNullable(member.extractOfficialEmail()).orElse("");
            String constituency = Optional.ofNullable(member.extractConstituency()).orElse("");
            String urlImage = Optional.ofNullable(member.extractUrlImage()).orElse("");
            String debateSubject = Optional.ofNullable(speech.getSectionHeading()).orElse("");

            // TODO: Use properties file
            // TODO: Use own url !!!
            Link selfLink = new Link("http://data.riksdagen.se/anforande/" + speech.getDockId() + "-" +
                    speech.getSpeechNumber());

            speechesDTO.add(new SpeechDTO(speechId, dockDate, speaker, party, officialEmail, constituency, urlImage,
                    debateSubject, selfLink));
        }

        return speechesDTO;
    }
}
