package com.parliamentchallenge.merger.service.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

// TODO: try to configure ObjectMapper in the configuration class so this note is no longer needed
@JsonIgnoreProperties(ignoreUnknown = true)
public class Speech {

    @JsonProperty("anforande_id")
    private String speechId;

    @JsonProperty("avsnittsrubrik")
    private String sectionHeading;

    @JsonProperty("dok_id")
    private String dockId;

    @JsonProperty("anforande_nummer")
    private String speechNumber;

    @JsonProperty("dok_datum")
    private String dockDate;

    @JsonProperty("talare")
    private String speaker;

    @JsonProperty("parti")
    private String party;

    @JsonProperty("intressent_id")
    private String stakeholderId;


    public Speech() {
    }

    public Speech(String speechId, String sectionHeading, String dockId, String speechNumber, String dockDate, String speaker, String party, String stakeholderId) {
        this.speechId = speechId;
        this.sectionHeading = sectionHeading;
        this.dockId = dockId;
        this.speechNumber = speechNumber;
        this.dockDate = dockDate;
        this.speaker = speaker;
        this.party = party;
        this.stakeholderId = stakeholderId;
    }

    public String getSpeechId() {
        return speechId;
    }

    public void setSpeechId(String speechId) {
        this.speechId = speechId;
    }

    public String getSectionHeading() {
        return sectionHeading;
    }

    public void setSectionHeading(String sectionHeading) {
        this.sectionHeading = sectionHeading;
    }

    public String getDockId() {
        return dockId;
    }

    public void setDockId(String dockId) {
        this.dockId = dockId;
    }

    public String getSpeechNumber() {
        return speechNumber;
    }

    public void setSpeechNumber(String speechNumber) {
        this.speechNumber = speechNumber;
    }

    public String getDockDate() {
        return dockDate;
    }

    public void setDockDate(String dockDate) {
        this.dockDate = dockDate;
    }

    public String getSpeaker() {
        return speaker;
    }

    public void setSpeaker(String speaker) {
        this.speaker = speaker;
    }

    public String getParty() {
        return party;
    }

    public void setParty(String party) {
        this.party = party;
    }

    public String getStakeholderId() {
        return stakeholderId;
    }

    public void setStakeholderId(String stakeholderId) {
        this.stakeholderId = stakeholderId;
    }

    @Override
    public String toString() {
        return "Speech{" +
                "speechId='" + speechId + '\'' +
                ", sectionHeading='" + sectionHeading + '\'' +
                ", dockId='" + dockId + '\'' +
                ", speechNumber='" + speechNumber + '\'' +
                ", dockDate='" + dockDate + '\'' +
                ", speaker='" + speaker + '\'' +
                ", party='" + party + '\'' +
                ", stakeholderId='" + stakeholderId + '\'' +
                '}';
    }
}
