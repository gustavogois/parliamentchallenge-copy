package com.parliamentchallenge.merger.service.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.parliamentchallenge.merger.util.JsonUtil;

// TODO: try to configure ObjectMapper in the configuration class so this note is no longer needed
@JsonIgnoreProperties(ignoreUnknown = true)
public class Member {

    private String officialEmail;
    private String constituency;
    private String urlImage;

    public Member() {
    }

    public Member(String officialEmail, String constituency, String urlImage) {
        this.officialEmail = officialEmail;
        this.constituency = constituency;
        this.urlImage = urlImage;
    }

    public static Member createFromJson(String json) {
        return new Member(extractOfficialEmail(json), extractConstituency(json), extractUrlImage(json));
    }

    private static String extractUrlImage(String json) {
        // TODO: Use properties file
        String evaluator = "$.personlista.person.bild_url_192";
        return JsonUtil.getFieldValue(json, evaluator, "bild_url_192");
    }

    private static String extractConstituency(String json) {
        // TODO: Use properties file
        String evaluator = "$.personlista.person.valkrets";
        return JsonUtil.getFieldValue(json, evaluator, "valkrets");
    }

    private static String extractOfficialEmail(String json) {
        // TODO: Use properties file
        String evaluator = "$..uppgift[?(@.kod == 'Officiell e-postadress')]";
        return JsonUtil.getFieldValue(json, evaluator, "uppgift");
    }

    public String extractOfficialEmail() {
        return officialEmail;
    }

    public void setOfficialEmail(String officialEmail) {
        this.officialEmail = officialEmail;
    }

    public String extractConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public String extractUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }
}
