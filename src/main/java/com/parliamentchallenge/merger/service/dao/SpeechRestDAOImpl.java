package com.parliamentchallenge.merger.service.dao;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.parliamentchallenge.merger.service.model.Member;
import com.parliamentchallenge.merger.service.model.Speech;
import com.parliamentchallenge.merger.util.SpeechResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.List;

@Component
public class SpeechRestDAOImpl implements SpeechDAO {

    @Autowired
    RestTemplate restTemplate;

    @Autowired
    ObjectMapper objectMapper;

    public String getMembers(String stakeholderId) {
        String urlMember = "http://data.riksdagen.se/personlista/?iid=" + stakeholderId +"&utformat=json";
        return restTemplate.getForObject(urlMember, String.class);
    }

    @Override
    // TODO: The Integration Test
    public List<Speech> loadTenLastSpeeches() throws IOException {

        // TODO: Put in a property file
        String urlSpeech = "http://data.riksdagen.se/anforandelista/?anftyp=Nej&sz=10&utformat=json";
        String json = restTemplate.getForObject(urlSpeech, String.class);

        // TODO: Change for Log use before deliver
        //System.out.println("SPEECH: " + json);

        SpeechResponse speechResponse = objectMapper.readerFor(SpeechResponse.class).readValue(json);

        // TODO: NPE
        return speechResponse.getSpeechResponseElement().getSpeedList();

    }

    @Override
    public Member findMemberSpeech(String stakeholderId) throws IOException {

        // TODO: Put in a property file
        String urlMember = "http://data.riksdagen.se/personlista/?iid=" + stakeholderId +"&utformat=json";
        String json = restTemplate.getForObject(urlMember, String.class);

        // TODO: Change for Log use before deliver
        //System.out.println("MEMBER: " + json);

        Member member = Member.createFromJson(json);

        return member;
    }
}
