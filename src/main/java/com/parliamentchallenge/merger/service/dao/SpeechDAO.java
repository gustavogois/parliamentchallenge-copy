package com.parliamentchallenge.merger.service.dao;

import com.parliamentchallenge.merger.service.model.Member;
import com.parliamentchallenge.merger.service.model.Speech;

import java.io.IOException;
import java.util.List;

// TODO: Javadoc

public interface SpeechDAO {
    List<Speech> loadTenLastSpeeches() throws IOException;
    Member findMemberSpeech(String stakeholderId) throws IOException;
}
