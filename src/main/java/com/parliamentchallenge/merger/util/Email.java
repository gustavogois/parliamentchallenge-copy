package com.parliamentchallenge.merger.util;

import java.util.ArrayList;

public class Email {

    public ArrayList<String> emails;

    public Email() {
    }

    public Email(ArrayList<String> emails) {
        this.emails = emails;
    }

    public ArrayList<String> getEmails() {
        return emails;
    }

    public void setEmails(ArrayList<String> emails) {
        this.emails = emails;
    }
}
