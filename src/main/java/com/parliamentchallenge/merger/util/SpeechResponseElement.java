package com.parliamentchallenge.merger.util;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.parliamentchallenge.merger.service.model.Speech;

import java.util.List;

public class SpeechResponseElement {

    @JsonProperty("@antal")
    private String quantity;

    @JsonProperty("anforande")
    private List<Speech> speedList;

    public SpeechResponseElement() {
    }

    public SpeechResponseElement(List<Speech> speedList) {
        this.speedList = speedList;
    }

    public List<Speech> getSpeedList() {
        return speedList;
    }

    public void setSpeedList(List<Speech> speedList) {
        this.speedList = speedList;
    }
}
