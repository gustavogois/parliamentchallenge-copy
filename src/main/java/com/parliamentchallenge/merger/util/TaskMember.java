package com.parliamentchallenge.merger.util;

import com.fasterxml.jackson.annotation.JsonProperty;

public class TaskMember {

    // TODO: Translate to english
    private String kod;
    @JsonProperty("uppgift")
    private Email email;
    private String typ;
    private String intressent_id;
    private String hangar_id;

    public TaskMember() {
    }

    public TaskMember(String kod, Email email, String typ, String intressent_id, String hangar_id) {
        this.kod = kod;
        this.email = email;
        this.typ = typ;
        this.intressent_id = intressent_id;
        this.hangar_id = hangar_id;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public Email getEmail() {
        return email;
    }

    public void setEmail(Email email) {
        this.email = email;
    }

    public String getTyp() {
        return typ;
    }

    public void setTyp(String typ) {
        this.typ = typ;
    }

    public String getIntressent_id() {
        return intressent_id;
    }

    public void setIntressent_id(String intressent_id) {
        this.intressent_id = intressent_id;
    }

    public String getHangar_id() {
        return hangar_id;
    }

    public void setHangar_id(String hangar_id) {
        this.hangar_id = hangar_id;
    }
}
