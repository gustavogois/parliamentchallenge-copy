package com.parliamentchallenge.merger.dto;

import java.util.List;

public class SpeechesDTO {

    private List<SpeechDTO> speechesDTOS;

    public SpeechesDTO() {
    }

    public SpeechesDTO(List<SpeechDTO> speechesDTOS) {
        this.speechesDTOS = speechesDTOS;
    }

    public List<SpeechDTO> getSpeechesDTOS() {
        return speechesDTOS;
    }

    public void setSpeechesDTOS(List<SpeechDTO> speechesDTOS) {
        this.speechesDTOS = speechesDTOS;
    }
}
