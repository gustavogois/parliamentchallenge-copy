package com.parliamentchallenge.merger.dto;

import org.springframework.hateoas.Link;

public class SpeechDTO {

    private String speechId;
    private String speechDate;
    private String speakerName;
    private String politicalAffiliation;
    private String officialEmail;
    private String constituency;
    private String urlImage;
    private String debateSubject;
    private Link selfLink;

    public SpeechDTO() {
    }

    public SpeechDTO(String speechId, String speechDate, String speakerName, String politicalAffiliation,
                     String officialEmail, String constituency, String urlImage, String debateSubject, Link selfLink) {
        this.speechId = speechId;
        this.speechDate = speechDate;
        this.speakerName = speakerName;
        this.politicalAffiliation = politicalAffiliation;
        this.officialEmail = officialEmail;
        this.constituency = constituency;
        this.urlImage = urlImage;
        this.debateSubject = debateSubject;
        this.selfLink = selfLink;
    }

    public String getSpeechId() {
        return speechId;
    }

    public void setSpeechId(String speechId) {
        this.speechId = speechId;
    }

    public String getSpeechDate() {
        return speechDate;
    }

    public void setSpeechDate(String speechDate) {
        this.speechDate = speechDate;
    }

    public String getSpeakerName() {
        return speakerName;
    }

    public void setSpeakerName(String speakerName) {
        this.speakerName = speakerName;
    }

    public String getPoliticalAffiliation() {
        return politicalAffiliation;
    }

    public void setPoliticalAffiliation(String politicalAffiliation) {
        this.politicalAffiliation = politicalAffiliation;
    }

    public String getOfficialEmail() {
        return officialEmail;
    }

    public void setOfficialEmail(String officialEmail) {
        this.officialEmail = officialEmail;
    }

    public String getConstituency() {
        return constituency;
    }

    public void setConstituency(String constituency) {
        this.constituency = constituency;
    }

    public String getUrlImage() {
        return urlImage;
    }

    public void setUrlImage(String urlImage) {
        this.urlImage = urlImage;
    }

    public String getDebateSubject() {
        return debateSubject;
    }

    public void setDebateSubject(String debateSubject) {
        this.debateSubject = debateSubject;
    }

    public Link getSelfLink() {
        return selfLink;
    }

    public void setSelfLink(Link selfLink) {
        this.selfLink = selfLink;
    }
}
