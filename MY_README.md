# My Notes

I respected the duration established to do the test: 6 hours.

# Parliament Challenge

This is a coding challenge using open API services to make a composite result.

I think I have met the minimum requirement: to return a series of data regarding speeches, using information from 2 APIs provided by the Swedish parliament.

If I had more time, I would do (described in TODOs):
- Exception handling
- Use property file to literals
- Swagger documentation
- Some translations and overal revision

## What do we Expect From you

 - Spend 4-6 hours on the task. **OK** 
 - As a bare minimum we want to be able to send a request and get a response with the merged data. **OK**
 - What would you do to deploy this in a production environment? 
    If there are things you want to add but you run out of time, please document your thoughts somehow! 
    - Ideally, we should do the build and deploy using CI tools (maven, jenkins, docker, etc.), first in an approval environment.
      I recall that unit (every small change - TDD) and integration tests (at least once or twice a day) should be performed throughout development.
      Then, once approved in the approval environment, we would use the CI tools to deploy in production.
      For this, it is essential to use environment variables in the development and testing code to consider the different environments.
 - Don't excuse yourself. Make code you can stand for. That is what we will judge you on in the end, right? 
 - But most importantly, enjoy the coding and have fun! **I enjoyed**

Good luck, may Godspeed be with You!


 